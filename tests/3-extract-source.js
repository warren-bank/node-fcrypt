var fcrypt = require('../')

var path = require('path')

var param = {
  key:      'mySuperPass1337',
  input:    path.join(__dirname, 'output', 'source.zip.aes-256-cbc'),
  output:   path.join(__dirname, 'output', 'source'),
  callback: function(errors){console.log('Errors:', JSON.stringify(errors, null, 2))},
  method:   'aes-256-cbc'
}

fcrypt.extract(param)
