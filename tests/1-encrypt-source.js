var fcrypt = require('../')

var path = require('path')

var param = {
  key:      'mySuperPass1337',
  input:    path.join(__dirname, '..', 'source'),
  output:   path.join(__dirname, 'output', 'source.zip.aes-256-cbc'),
  callback: function(errors){console.log('Errors:', JSON.stringify(errors, null, 2))},
  method:   'aes-256-cbc'
}

fcrypt.encrypt(param)
